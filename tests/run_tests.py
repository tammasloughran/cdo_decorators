#!/usr/bin/env python3
import io
import sys
import types
import unittest
from contextlib import redirect_stdout

import netCDF4 as nc
import numpy as np
from cdo import Cdo
import cdo as cdom

import cdo_decorators as cdod

sys.path.insert(0, '../')
cdo = Cdo()
cdo.debug = True


def make_example_file():
    ncfile = nc.Dataset('example.nc', 'w')
    ncfile.createDimension('longitude', size=3)
    ncfile.createDimension('latitude', size=3)
    ncfile.createDimension('time', size=None)
    example = ncfile.createVariable('example_variable', 'f',
            dimensions=('time', 'latitude', 'longitude'))
    lats = ncfile.createVariable('latitude', 'f', dimensions=('latitude'))
    lons = ncfile.createVariable('longitude', 'f', dimensions=('longitude'))
    times = ncfile.createVariable('time', 'f', dimensions=('time'))
    example[:] = np.ones((1,3,3), dtype=float)
    lats[:] = np.array([10,20,30], dtype=float)
    lons[:] = np.array([10,20,30], dtype=float)
    times[:] = np.array([10,20,30], dtype=float)
    ncfile.close()


class TestSingleInputFunction:

    def __init__(self, cdo_decorator:types.FunctionType):
        self.cdo_decorator = cdo_decorator
        self.name = cdo_decorator._name
        self.expected = f'cdo -O -copy -{self.name} example.nc foo.nc'

    def __call__(self):

        @self.cdo_decorator
        def operation(input:str, output:str):
            return cdo.copy(input=input, output=output)

        try:
            f = io.StringIO()
            with redirect_stdout(f):
                operation(input='example.nc', output='foo.nc')
            out = f.getvalue()
            out = out.split('\n')
            out = [o for o in out if 'CALL' in o][0]
            result = out[7:]
            assert result==self.expected
        except AssertionError:
            print(result)
            print('should be:')
            print(self.expected)
        except cdom.CDOException:
            print(f'{self.name} failed')
            pass
        print(f"{self.name} success")


class TestCdoFunction(unittest.TestCase):

    def __init__(self):
        commands = [c for c in cdod.decorators.__all__ if 'cdo_' in c]
        self.local_vars = locals()
        for command in cdod.decorators._single_input_cdo_commands:
            self.local_vars.update({'test_command_'+command:TestSingleInputFunction(command)})


make_example_file()
commands = {'cdo_'+c:getattr(cdod, 'cdo_'+c) for c in cdod.decorators._single_input_cdo_commands}
for key,func in commands.items():
    test_command = TestSingleInputFunction(func)
    test_command()

