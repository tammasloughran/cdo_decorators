# cdo_decorators

This library is a collection of decorators for constructing CDO command chains.

## API Documentation

https://tammasloughran.gitlab.io/cdo_decorators/

## Installation

```bash
# Set up a vitual environment (optional)
virtualenv myenv
source ./myenv/bin/activate
# Install
pip install git+https://gitlab.com/tammasloughran/cdo_decorators.git
```

## Decorators

Decorators are functions that modify the execution of existing functions.
This is possible in Python because functions are treated as first-class objects.
In other words, function identifiers can be passed as arguments to other functions and returned
from functions, just like any other variable identifier.
By passing a function into another, you can define a new function that executes code before or
after a function is executed, thus adding, or "wrapping", new functionality.
Python provides syntactic sugar to do this in a concise way.
The syntax consists of a line preceding a function definition that specifies the decorating
function with an "`@`" symbol.

## Basic usage of decorators in Python

The basic structure for writing a decorator in python is as follows:

```python
import functools

def decorator_f(func):
    @functools.wraps(func)
    def wrapper_f(*args, **kwargs):
        # Do something before func execution.
        result = func(*args, **kwargs)
        # Do something after func execution.
        return result
    return wrapper_f
```

The `functools` module provides a decorator that allows any docstrings and identifiers in the
original function to be preserved and passed through to the final wrapped function. This is done 
using `functools.wraps()`. The use of `*args` and `**kwargs`, passes through all the arguments
from the original function through to the wrapper.
 
The newly written decorator can then be used to decorate other functions using the "`@`" syntax.

```python
@decorator_f
def another_function()
    return "Hello, World!"
```

## CDO

Climate Data Operators (CDO) has a python interface that relies on system calls to an existing
installation of `cdo`. The cdo module provides functions for each operator available in CDO and
allows for operation chaining, convenient temporary file management and netCDF file loading.
Chaining is achieved by passing chained commands as they would occur in a regular CDO command to
the `input` keyword argument of a cdo python function.

```python
import cdo as cdo_module

cdo = cdo_module.Cdo()

data = cdo.mulc(100, input='-fldsum infile.nc', returnCdf=True).variables['var'][:]
```

The problem with this (and also with using CDO in general), is that the cdo command chain string
can become very long, making them difficult to read and understand what operations are being
done and when. CDO command chains are generally executed from right to left, which is counterintuitive.

## CDO and Python decorators, together at last

I typically use decorators to so that I can program CDO command chains in a clearer way, i.e. as a
vertical list of operations. Each function should represent an idea of an operation on an .nc file.
This is often a single cdo command, but may be a group of chained commands.
For example, multiplying a .nc file by a unit conversion factor. Or perhaps multiplying by the land
surface area.

```python
import functools


def cdo_kg2pg(cdo_func):
    """Wrapper to add conversion factors from kg to Pg to the CDO command.
    """
    @functools.wraps(cdo_func)
    def wrapper_kg2pg(*args, **kwargs):
        cdo_string = f'-divc,{KG_IN_PG} ' + kwargs['input']
        return cdo_func(*args, **kwargs)
    return wrapper_kg2pg   


def cdo_mul_land_area(cdo_func):
    """Wrapper to add multiplication of the land area and land cover fractions to the CDO command.
    """
    @functools.wraps(cdo_func)
    def wrapper_mul_land_area(*args, **kwargs):
        cdo_string = '-mul -mul ' + kwargs['input'] + \
                f' -divc,100 {LAND_FRAC_FILE} -gridarea {LAND_FRAC_FILE}'
        return cdo_func(*args, **kwargs)
    return wrapper_mul_land_area
```

These wrapper functions are more modular and easily digestible compared to writing a single long
CDO command chain. The order and the ideas of the functionality that they represent are clearer
when using them to decorate functions.

```python
@cdo_mul_land_area
@cdo_kg2pg
def cdo_load(input:str, var:str):
    """Load a netCDF file using CDO.
    """
    return cdo.copy(input=input, returnCdf=True).variables[var][:]

data = cdo_load(input='infile.nc', var='cVeg')
```

In this example, when `cdo_load()` is called, the arguments are passed first to
`cdo_mul_land_area`, where the CDO '-mul' commands are added to the input string and then those
arguments are passed to `cdo_kg2pg()` where the units conversion is added, before finally executing
`cdo_load()`.

## Example Usage

### Single input commands

```python
from cdo import Cdo
import cdo_decorators as cdod

cdo = Cdo()

@cdod.cdo_fldsum
def cdo_loader(input:str, var:str):
    return cdo.copy(input=input, returnCdf=True).variables[var][:].squeeze()

data = cdo_loader(input='example.nc', var='example_variable')
```

### Single input with parameter arguments

Parameter arguments are specified as positional arguments of the decorator.

```python
from cdo import Cdo
import cdo_decorators as cdod

cdo = Cdo()

KG_IN_PG = 1000000000000

@cdod.cdo_fldsum
@cdod.cdo_divc(str(KG_IN_PG))
def cdo_loader(input:str, var:str):
    return cdo.copy(input=input, returnCdf=True).variables[var][:].squeeze()

data = cdo_loader(input='example.nc', var='example_variable')
```

### Multiple input files

CDO commands that need two input files can also be specified with keyword arguments.
The omitted decorator keyword argument is substituted with the `input` keyword argument from the
decorated function (i.e. 'example.nc'). You can choose which position the inputs should be in
by using the alternative `input1` or `input2` decorator keyword argument.

```python
from cdo import Cdo
import cdo_decorators as cdod

cdo = Cdo()

KG_IN_PG = 1000000000000
LAND_FRAC_FILE = 'stfl.nc'

@cdod.cdo_ifthen(input1=LAND_FRAC_FILE)
@cdod.cdo_fldsum
@cdod.cdo_divc(str(KG_IN_PG))
def cdo_loader(input:str, var:str):
    return cdo.copy(input=input, returnCdf=True).variables[var][:].squeeze()

data = cdo_loader(input='example.nc', var='example_variable')
```

## Common mistakes

### `KeyError: 'input'`

You put your input file in a positional argument.
This is not allowed in the standard python CDO library.
You **must** specify input files using the `input` keyword argument.
If you put input files in an ordered function argument, it will be passed into the *args argument
of the decorator instead of the **kwargs argument.

### `AttributeError: 'str' object has no attribute 'variables'`

The CDO library returns a string of the full directory and filename of the output file if you
do not specify it to return a netCDF object.
But you have probably treated that string as a netCDF object anyway if you're using the `variables`
attribute.
This is likely becuase of a typo in the `returnCdf` keyword argument.
It fails silently because python thinks that the keyword argument with the typo is an "arbitrary
keyword argument" and not one that is already defined in the function definition.
It is very easy to miss-type `returnCdf=True` as `returnCDF=True`, which is incorrect.

## List of available CDO command decorators

### Single input decorators

`cdo_abs`  
`cdo_acos`  
`cdo_asin`  
`cdo_atan`  
`cdo_bottomvalue`  
`cdo_consecsum`  
`cdo_consects`  
`cdo_copy`  
`cdo_copy`  
`cdo_cos`  
`cdo_dayavg`  
`cdo_daymax`  
`cdo_daymean`  
`cdo_daymin`  
`cdo_dayrange`  
`cdo_daystd`  
`cdo_daystd1`  
`cdo_daysum`  
`cdo_dayvar`  
`cdo_dayvar1`  
`cdo_dhouravg`  
`cdo_dhourmax`  
`cdo_dhourmean`  
`cdo_dhourmin`  
`cdo_dhourrange`  
`cdo_dhourstd`  
`cdo_dhourstd1`  
`cdo_dhoursum`  
`cdo_dhourvar`  
`cdo_dhourvar1`  
`cdo_divcoslat`  
`cdo_divdpm`  
`cdo_divdpy`  
`cdo_ensavg`  
`cdo_enskurt`  
`cdo_ensmax`  
`cdo_ensmean`  
`cdo_ensmedian`  
`cdo_ensmin`  
`cdo_ensrange`  
`cdo_ensskew`  
`cdo_ensstd`  
`cdo_ensstd1`  
`cdo_enssum`  
`cdo_ensvar`  
`cdo_ensvar1`  
`cdo_exp`  
`cdo_fldavg`  
`cdo_fldkurt`  
`cdo_fldmax`  
`cdo_fldmean`  
`cdo_fldmedian`  
`cdo_fldmin`  
`cdo_fldrange`  
`cdo_fldskew`  
`cdo_fldstd`  
`cdo_fldstd1`  
`cdo_fldsum`  
`cdo_fldvar`  
`cdo_fldvar1`  
`cdo_gridarea`  
`cdo_houravg`  
`cdo_hourmax`  
`cdo_hourmean`  
`cdo_hourmin`  
`cdo_hourrange`  
`cdo_hourstd`  
`cdo_hourstd1`  
`cdo_hoursum`  
`cdo_hourvar`  
`cdo_hourvar1`  
`cdo_int`  
`cdo_invertlat`  
`cdo_invertlev`  
`cdo_ln`  
`cdo_log10`  
`cdo_meravg`  
`cdo_merkurt`  
`cdo_mermax`  
`cdo_mermean`  
`cdo_mermedian`  
`cdo_mermin`  
`cdo_merrange`  
`cdo_merskew`  
`cdo_merstd`  
`cdo_merstd1`  
`cdo_mersum`  
`cdo_mervar`  
`cdo_mervar1`  
`cdo_monavg`  
`cdo_monmax`  
`cdo_monmean`  
`cdo_monmin`  
`cdo_monrange`  
`cdo_monstd`  
`cdo_monstd1`  
`cdo_monsum`  
`cdo_monvar`  
`cdo_monvar1`  
`cdo_mulcoslat`  
`cdo_muldpm`  
`cdo_muldpy`  
`cdo_ndate`  
`cdo_ngridpoints`  
`cdo_ngrids`  
`cdo_nint`  
`cdo_nlevel`  
`cdo_nmon`  
`cdo_not`  
`cdo_npar`  
`cdo_ntime`  
`cdo_nyear`  
`cdo_pack`  
`cdo_reci`  
`cdo_seasavg`  
`cdo_seasmax`  
`cdo_seasmean`  
`cdo_seasmin`  
`cdo_seasrange`  
`cdo_seasstd`  
`cdo_seasstd1`  
`cdo_seassum`  
`cdo_seasvar`  
`cdo_seasvar1`  
`cdo_showcode`  
`cdo_showdate`  
`cdo_showformat`  
`cdo_showlevel`  
`cdo_showltype`  
`cdo_showmon`  
`cdo_showname`  
`cdo_showstdname`  
`cdo_showtime`  
`cdo_showtimestamp`  
`cdo_showyear`  
`cdo_sin`  
`cdo_splitday`  
`cdo_splithour`  
`cdo_splitseas`  
`cdo_splityear`  
`cdo_splityearmon`  
`cdo_sqr`  
`cdo_tan`  
`cdo_timavg`  
`cdo_timcumsum`  
`cdo_timmax`  
`cdo_timmean`  
`cdo_timmin`  
`cdo_timrange`  
`cdo_timstd`  
`cdo_timstd1`  
`cdo_timsum`  
`cdo_timvar`  
`cdo_timvar1`  
`cdo_topvalue`  
`cdo_varsavg`  
`cdo_varsmax`  
`cdo_varsmean`  
`cdo_varsmin`  
`cdo_varsrange`  
`cdo_varsstd`  
`cdo_varsstd1`  
`cdo_varssum`  
`cdo_varsvar`  
`cdo_varsvar1`  
`cdo_ydayavg`  
`cdo_ydaymax`  
`cdo_ydaymean`  
`cdo_ydaymin`  
`cdo_ydayrange`  
`cdo_ydaystd`  
`cdo_ydaystd1`  
`cdo_ydaysum`  
`cdo_ydayvar`  
`cdo_ydayvar1`  
`cdo_yearavg`  
`cdo_yearmax`  
`cdo_yearmaxidx`  
`cdo_yearmean`  
`cdo_yearmin`  
`cdo_yearminidx`  
`cdo_yearmonmean`  
`cdo_yearrange`  
`cdo_yearstd`  
`cdo_yearstd1`  
`cdo_yearsum`  
`cdo_yearvar`  
`cdo_yearvar1`  
`cdo_yhouravg`  
`cdo_yhourmax`  
`cdo_yhourmean`  
`cdo_yhourmin`  
`cdo_yhourrange`  
`cdo_yhourstd`  
`cdo_yhourstd1`  
`cdo_yhoursum`  
`cdo_yhourvar`  
`cdo_yhourvar1`  
`cdo_ymonavg`  
`cdo_ymonmax`  
`cdo_ymonmean`  
`cdo_ymonmin`  
`cdo_ymonrange`  
`cdo_ymonstd`  
`cdo_ymonstd1`  
`cdo_ymonsum`  
`cdo_ymonvar`  
`cdo_ymonvar1`  
`cdo_yseasavg`  
`cdo_yseasmax`  
`cdo_yseasmean`  
`cdo_yseasmin`  
`cdo_yseasrange`  
`cdo_yseasstd`  
`cdo_yseasstd1`  
`cdo_yseassum`  
`cdo_yseasvar`  
`cdo_yseasvar1`  
`cdo_zonavg`  
`cdo_zonkurt`  
`cdo_zonmax`  
`cdo_zonmean`  
`cdo_zonmedian`  
`cdo_zonmin`  
`cdo_zonrange`  
`cdo_zonskew`  
`cdo_zonstd`  
`cdo_zonstd1`  
`cdo_zonsum`  
`cdo_zonvar`  
`cdo_zonvar1`]

### Two inputs decorators

`cdo_add`  
`cdo_atan2`  
`cdo_cat`  
`cdo_dayadd`  
`cdo_daydiv`  
`cdo_daymul`  
`cdo_daysub`  
`cdo_div`  
`cdo_eofcoeff`  
`cdo_eq`  
`cdo_fdns`  
`cdo_fldcor`  
`cdo_fldcovar`  
`cdo_ge`  
`cdo_gt`  
`cdo_ifnotthen`  
`cdo_ifthen`  
`cdo_le`  
`cdo_lt`  
`cdo_max`
`cdo_mergegrid`  
`cdo_min`  
`cdo_monadd`  
`cdo_mondiv`  
`cdo_monmul`  
`cdo_monsub`  
`cdo_mrotuvb`  
`cdo_mul`  
`cdo_ne`  
`cdo_replace`  
`cdo_selyearidx`  
`cdo_sub`  
`cdo_timcor`  
`cdo_timcovar`  
`cdo_wtc`  
`cdo_ydayadd`  
`cdo_ydaydiv`  
`cdo_ydaymul`  
`cdo_ydaysub`  
`cdo_yearadd`  
`cdo_yeardiv`  
`cdo_yearmul`  
`cdo_yearsub`  
`cdo_yhouradd`  
`cdo_yhourdiv`  
`cdo_yhourmul`  
`cdo_yhoursub`  
`cdo_ymonadd`  
`cdo_ymondiv`  
`cdo_ymonmul`  
`cdo_ymonsub`  
`cdo_yseasadd`  
`cdo_yseasdiv`  
`cdo_yseasmul`  
`cdo_yseassub`]

### Parameter arguments decorators

`cdo_addc`  
`cdo_aexpr`  
`cdo_aexprf`  
`cdo_ap2pl`  
`cdo_apply`  
`cdo_changemulti`  
`cdo_chcode`  
`cdo_chlevel`  
`cdo_chlevelc`  
`cdo_chlevelv`  
`cdo_chname`  
`cdo_chparam`  
`cdo_chunit`  
`cdo_collgrid`  
`cdo_delcode`  
`cdo_delete`  
`cdo_delgridcell`  
`cdo_delmulti`  
`cdo_delmulti`  
`cdo_delname`  
`cdo_delparam`  
`cdo_distgrid`  
`cdo_divc`  
`cdo_enlarge`  
`cdo_enspctl`  
`cdo_eqc`  
`cdo_expr`  
`cdo_exprf`  
`cdo_fldpctl`  
`cdo_gec`  
`cdo_genbic`  
`cdo_genbil`  
`cdo_gencon`  
`cdo_gencon2`  
`cdo_gendis`  
`cdo_genlaf`  
`cdo_genlevelbounds`  
`cdo_gennn`  
`cdo_gh2hl`  
`cdo_gtc`  
`cdo_intlevel`  
`cdo_intntime`  
`cdo_inttime`  
`cdo_isosurface`  
`cdo_lec`  
`cdo_ltc`  
`cdo_maskindexbox`  
`cdo_masklonlatbox`  
`cdo_maskregion`  
`cdo_maxc`  
`cdo_merpctl`  
`cdo_minc`  
`cdo_ml2hl`  
`cdo_ml2pl`  
`cdo_mulc`  
`cdo_nec`  
`cdo_pow`  
`cdo_reducegrid`  
`cdo_remap`  
`cdo_remapbic`  
`cdo_remapbil`  
`cdo_remapcon`  
`cdo_remapcon2`  
`cdo_remapdis`  
`cdo_remapeta`  
`cdo_remaplaf`  
`cdo_remapnn`  
`cdo_samplegrid`  
`cdo_selcircle`  
`cdo_selcode`  
`cdo_seldate`  
`cdo_selday`  
`cdo_select`  
`cdo_selgrid`  
`cdo_selgridcell`  
`cdo_selhour`  
`cdo_selindexbox`  
`cdo_sellevel`  
`cdo_sellevidx`  
`cdo_sellonlatbox`  
`cdo_selltype`  
`cdo_selmonth`  
`cdo_selmulti`  
`cdo_selmulti`  
`cdo_selname`  
`cdo_selparam`  
`cdo_selregion`  
`cdo_selseason`  
`cdo_selsmon`  
`cdo_selstdname`  
`cdo_seltabnum`  
`cdo_seltime`  
`cdo_seltimestep`  
`cdo_selvar`  
`cdo_selyear`  
`cdo_selzaxis`  
`cdo_selzaxisname`  
`cdo_setattribute`  
`cdo_setcalendar`  
`cdo_setcindexbox`  
`cdo_setclonlatbox`  
`cdo_setcode`  
`cdo_setcodetab`  
`cdo_setctomiss`  
`cdo_setdate`  
`cdo_setday`  
`cdo_setgrid`  
`cdo_setgridarea`  
`cdo_setgridcell`  
`cdo_setgridmask`  
`cdo_setgridtype`  
`cdo_setlevel`  
`cdo_setltype`  
`cdo_setmisstoc`  
`cdo_setmisstodis`  
`cdo_setmisstonn`  
`cdo_setmissval`  
`cdo_setmon`  
`cdo_setname`  
`cdo_setparam`  
`cdo_setpartabn`  
`cdo_setpartabp`  
`cdo_setreftime`  
`cdo_setrtomiss`  
`cdo_settaxis`  
`cdo_settbounds`  
`cdo_settime`  
`cdo_settunits`  
`cdo_setunit`  
`cdo_setvrange`  
`cdo_setyear`  
`cdo_setzaxis`  
`cdo_shifttime`  
`cdo_shiftx`  
`cdo_shifty`  
`cdo_splitcode`  
`cdo_splitgrid`  
`cdo_splitlevel`  
`cdo_splitmon`  
`cdo_splitname`  
`cdo_splitparam`  
`cdo_splitsel`  
`cdo_splittabnum`  
`cdo_splitzaxis`  
`cdo_subc`  
`cdo_vertavg`  
`cdo_vertmax`  
`cdo_vertmean`  
`cdo_vertmin`  
`cdo_vertrange`  
`cdo_vertstd`  
`cdo_vertstd1`  
`cdo_vertsum`  
`cdo_vertvar`  
`cdo_vertvar1`  
`cdo_ydrunavg`  
`cdo_ydrunmax`  
`cdo_ydrunmean`  
`cdo_ydrunmin`  
`cdo_ydrunrange`  
`cdo_ydrunstd`  
`cdo_ydrunstd1`  
`cdo_ydrunsum`  
`cdo_ydrunvar`  
`cdo_ydrunvar1`  
`cdo_zonpctl`

### Two inputs and parameter argument decorators

`cdo_diff`  
`cdo_diffn`  
`cdo_intlevel3d`  
`cdo_intlevelx3d`  
`cdo_intyear`

### Three inputs and parameter arguments decorators

`cdo_timpctl`  
`cdo_hourpctl`  
`cdo_daypctl`  
`cdo_monpctl`  
`cdo_yearpctl`  
`cdo_seaspctl`  
`cdo_ydaypctl`  
`cdo_ymonpctl`  
`cdo_yseaspctl`
 
### Three inputs decorators

`cdo_ifthenelse`

## Acknowledgments

Many thanks to Jacob Neil Taylor for constructive feedback and suggestions. <3

