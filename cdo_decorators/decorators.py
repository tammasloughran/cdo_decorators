# CDO command decorators
import functools
from types import FunctionType

# Single input file commands. These have the form `cdo command infile outfile`
_single_input_cdo_commands = ['abs','acos','asin','atan','bottomvalue','consecsum','consects',
        'copy','copy','cos','dayavg','daymax','daymean','daymin','dayrange','daystd','daystd1',
        'daysum','dayvar','dayvar1','deltat','dhouravg','dhourmax','dhourmean','dhourmin','dhourrange',
        'dhourstd','dhourstd1','dhoursum','dhourvar','dhourvar1','divcoslat','divdpm','divdpy',
        'ensavg','enskurt','ensmax','ensmean','ensmedian','ensmin','ensrange','ensskew','ensstd',
        'ensstd1','enssum','ensvar','ensvar1','exp','fldmax','fldmin','fldrange','fldsum',
        'gridarea','houravg','hourmax','hourmean','hourmin','hourrange','hourstd','hourstd1',
        'hoursum','hourvar','hourvar1','int','invertlat','invertlev','ln','log10','meravg',
        'merkurt','mermax','mermean','mermedian','mermin','merrange','merskew','merstd','merstd1',
        'mersum','mervar','mervar1','monavg','monmax','monmean','monmin','monrange','monstd',
        'monstd1','monsum','monvar','monvar1','mulcoslat','muldpm','muldpy','ndate','ngridpoints',
        'ngrids','nint','nlevel','nmon','not','npar','ntime','nyear','pack','reci','seasavg',
        'seasmax','seasmean','seasmin','seasrange','seasstd','seasstd1','seassum','seasvar',
        'seasvar1','showcode','showdate','showformat','showlevel','showltype','showmon','showname',
        'showstdname','showtime','showtimestamp','showyear','sin','splitday','splithour',
        'splitseas','splityear','splityearmon','sqr','tan','timavg','timcumsum','timmax','timmean',
        'timmin','timrange','timstd','timstd1','timsum','timvar','timvar1','topvalue','varsavg',
        'varsmax','varsmean','varsmin','varsrange','varsstd','varsstd1','varssum','varsvar',
        'varsvar1','ydayavg','ydaymax','ydaymean','ydaymin','ydayrange',
        'ydaystd','ydaystd1','ydaysum','ydayvar','ydayvar1','yearavg','yearmax','yearmaxidx',
        'yearmean','yearmin','yearminidx','yearmonmean','yearrange','yearstd','yearstd1',
        'yearsum','yearvar','yearvar1','yhouravg','yhourmax','yhourmean','yhourmin','yhourrange',
        'yhourstd','yhourstd1','yhoursum','yhourvar','yhourvar1','ymonavg','ymonmax','ymonmean',
        'ymonmin','ymonrange','ymonstd','ymonstd1','ymonsum','ymonvar','ymonvar1','yseasavg',
        'yseasmax','yseasmean','yseasmin','yseasrange','yseasstd','yseasstd1','yseassum',
        'yseasvar','yseasvar1','zonavg','zonkurt','zonmax','zonmean','zonmedian','zonmin',
        'zonrange','zonskew','zonstd','zonstd1','zonsum','zonvar','zonvar1',
        'fldkurt','fldmedian','fldskew',
        ]

# Two input file commands. These have the form `cdo command infule1 infile2 outfile`
_two_inputs_cdo_commands = ['add','atan2','cat','dayadd','daydiv','daymul','daysub','div',
        'eofcoeff','eq','fdns','fldcor','fldcovar','ge','gt','ifnotthen','ifthen','le','lt','max'
        ,'mergegrid','min','monadd','mondiv','monmul','monsub','mrotuvb','mul','ne','replace',
        'selyearidx','sub','timcor','timcovar','wtc','ydayadd','ydaydiv','ydaymul','ydaysub',
        'yearadd','yeardiv','yearmul','yearsub','yhouradd','yhourdiv','yhourmul','yhoursub',
        'ymonadd','ymondiv','ymonmul','ymonsub','yseasadd','yseasdiv','yseasmul','yseassub',
        ]

# Commands with parameter arguments. These have the form `cdo command,arg[,arg2] infile outfile`
_arg_cdo_commands = ['addc','aexpr','aexprf','ap2pl','apply','changemulti','chcode','chlevel',
        'chlevelc','chlevelv','chname','chparam','chunit','collgrid','delcode','delete',
        'delgridcell','delmulti','delmulti','delname','delparam','distgrid','divc','enlarge',
        'enspctl','eqc','expr','exprf','fldpctl','gec','genbic','genbil','gencon','gencon2',
        'gendis','genlaf','genlevelbounds','gennn','gh2hl','gtc','intlevel','intntime','inttime',
        'isosurface','lec','ltc','maskindexbox','masklonlatbox','maskregion','maxc','merpctl',
        'minc','ml2hl','ml2pl','mulc','nec','pow','reducegrid','remap','remapbic','remapbil',
        'remapcon','remapcon2','remapdis','remapeta','remaplaf','remapnn','samplegrid','selcircle',
        'selcode','seldate','selday','select','selgrid','selgridcell','selhour','selindexbox',
        'sellevel','sellevidx','sellonlatbox','selltype','selmonth','selmulti','selmulti',
        'selname','selparam','selregion','selseason','selsmon','selstdname','seltabnum','seltime',
        'seltimestep','selvar','selyear','selzaxis','selzaxisname','setattribute','setcalendar',
        'setcindexbox','setclonlatbox','setcode','setcodetab','setctomiss','setdate','setday',
        'setgrid','setgridarea','setgridcell','setgridmask','setgridtype','setlevel','setltype',
        'setmisstoc','setmisstodis','setmisstonn','setmissval','setmon','setname','setparam',
        'setpartabn','setpartabp','setreftime','setrtomiss','settaxis','settbounds','settime',
        'settunits','setunit','setvrange','setyear','setzaxis','shifttime','shiftx','shifty',
        'splitcode','splitgrid','splitlevel','splitmon','splitname','splitparam','splitsel',
        'splittabnum','splitzaxis','subc','ydrunavg','ydrunmax','ydrunmean','ydrunmin',
        'ydrunrange','ydrunstd','ydrunstd1','ydrunsum','ydrunvar','ydrunvar1','zonpctl',
        'fldmean','vertmean','fldavg','fldstd','fldstd1','fldvar','fldvar1',
        'vertavg','vertmax','vertmin','vertrange','vertstd','vertstd1','vertsum','vertvar',
        'vertvar1',
        ]

# Arg and 2input files. Many of these have 3 input files.
_arg_two_inputs_cdo_commands = ['diff','diffn','intlevel3d','intlevelx3d','intyear']

# 3 inputs and args commands
#'timpctl','hourpctl','daypctl','monpctl','yearpctl','seaspctl','ydaypctl','ymonpctl','yseaspctl'
#'addtrend','subtrend',
# Arbitrary infile commands
#'merge'

# 3 input commands:
_three_inputs_cdo_commands = ['ifthenelse']

# Specify the objects to be imported with `from decorators import *`.
__all__ = ['cdo_'+c for c in _single_input_cdo_commands+\
        _two_inputs_cdo_commands+\
        _arg_cdo_commands+\
        _three_inputs_cdo_commands]
__all__ += ['get_str', 'CdoFunction', 'CdoSingleInputFunction', 'CdoTwoInputsFunction',
        'CdoArgInputsFunction', 'CdoThreeInputsFunction']

# Base class for CDO decorator functions.
class CdoFunction():
    """Base class for a CDO decorator function.
    Decorator function that wraps a CDO command chain with the -{name} command
    Please refer to the CDO documentation for more details. e.g. `cdo -h {name}`
    or in python: help(cdo.{name})
    """
    name: str

    def __init__(self, name:str):
        self._name = name
        self.__doc__ = f"""
                Decorator function that wraps a CDO command chain with the -{name} command
                Please refer to the CDO documentation for more details. e.g. `cdo -h {name}`
                or in python: help(cdo.{name})
                """

# Class for decorator functions that only need a single input file.
class CdoSingleInputFunction(CdoFunction):
    """Decorator function that wraps a CDO command chain with the -{name} command
    Please refer to the CDO documentation for more details. e.g. `cdo -h {name}`
    or in python: help(cdo.{name})
    """

    def __call__(self, cdo_func:FunctionType)->FunctionType:
        """This decorator uses the `input` keyword argument from the decorated function. Therefore,
        no decorator arguments are needed.
        """
        @functools.wraps(cdo_func)
        def wrapper(*args, **kwargs):
            kwargs['input'] = f'-{self._name} {kwargs["input"]}'
            return cdo_func(*args, **kwargs)
        return wrapper


# Class for decorator functions that need two input files.
class CdoTwoInputsFunction(CdoFunction):
    """Decorator function that wraps a CDO command chain with the -{name} command
    Please refer to the CDO documentation for more details. e.g. `cdo -h {name}`
    or in python: help(cdo.{name})
    """

    def __call__(self, input1:str=None, input2:str=None)->FunctionType:
        """Two input files are allowed via the decorator's arguments.
        `input1`, `input2`. One of these may be omited. The omitted input
        file is substituted with the cdo function's `input` keyword argument.
        """
        if (input1 is input2 is None):
            raise ValueError("Only one of input1 or input2 may be blank.")
        def decorator(cdo_func:FunctionType)->FunctionType:
            @functools.wraps(cdo_func)
            def wrapper(*args, **kwargs):
                if input1 is None:
                    # Python looks ahead for variable definitions within a scope to see if they
                    # are global or local. So I need to use new local-only variable names.
                    inputfile1 = kwargs['input']
                    inputfile2 = input2
                elif input2 is None:
                    inputfile1 = input1
                    inputfile2 = kwargs['input']
                kwargs['input'] = f'-{self._name} {inputfile1} {inputfile2}'
                return cdo_func(*args, **kwargs)
            return wrapper
        return decorator


# Class for decorator functions that need parameter arguments.
class CdoArgInputsFunction(CdoFunction):
    """Decorator function that wraps a CDO command chain with the -{name} command
    Please refer to the CDO documentation for more details. e.g. `cdo -h {name}`
    or in python: help(cdo.{name})
    """

    def __call__(self, *oargs, **okwargs):
        """`oargs` are passed to the parameter arguments of the decorating CDO
        command. The `input` keyword argument from the decorated function is used as the single
        input file for the command.
        """
        if (len(oargs) + len(okwargs))>0:
            comma = ','
        else:
            comma = ''
        try:
            cdo_args = ','.join(list(oargs)+[key+'='+val for key,val in okwargs.items()])
        except TypeError as FirstException:
            raise TypeError(
                    "I was expecting arguments and/or keyword arguments for a decorator.\n"
                    "Did you forget to put parentheses after a decorator that needs them?\n"
                    "eg @cdod.cdo_fldmean()"
                    )
        def decorator(cdo_func:FunctionType)->FunctionType:
            @functools.wraps(cdo_func)
            def wrapper(*iargs, **ikwargs):
                ikwargs['input'] = f'-{self._name}{comma}{cdo_args} {ikwargs["input"]}'
                return cdo_func(*iargs, **ikwargs)
            return wrapper
        return decorator


# Class for decorator functions that need parameter arguments and 2 input files.
class CdoArgTwoInputsFunction(CdoFunction):
    """Decorator function that wraps a CDO command chain with the -{name} command
    Please refer to the CDO documentation for more details. e.g. `cdo -h {name}`
    or in python: help(cdo.{name})
    """

    def __call__(self, input1:str=None, input2:str=None, *oargs, **okwargs):
        """`oargs` are passed to the parameter arguments of the decorating CDO
        command. The `input` keyword argument from the decorated function is used as the single
        input file for the command. Two input files are allowed via the decorator's arguments.
        `input1`, `input2`. One of these may be omited. The omitted input
        file is substituted with the cdo function's `input` keyword argument.
        """
        if (len(oargs) + len(okwargs))>0:
            comma = ','
        else:
            comma = ''
        cdo_args = ','.join(list(oargs)+[key+'='+val for key,val in okwargs.items()])
        def decorator(cdo_func:FunctionType)->FunctionType:
            @functools.wraps(cdo_func)
            def wrapper(*iargs, **ikwargs):
                if input1 is None:
                    # Python looks ahead for variable definitions within a scope to see if they
                    # are global or local. So I need to use new local-only variable names.
                    inputfile1 = ikwargs['input']
                    inputfile2 = input2
                elif input2 is None:
                    inputfile1 = input1
                    inputfile2 = ikwargs['input']
                ikwargs['input'] = f'-{self._name}{comma}{cdo_args} {inputfile1} {inputfile2}'
                return cdo_func(*iargs, **ikwargs)
            return wrapper
        return decorator


# Class for decorator functions that need three input arguments.
class CdoThreeInputsFunction(CdoFunction):
    """Decorator function that wraps a CDO command chain with the -{name} command
    Please refer to the CDO documentation for more details. e.g. `cdo -h {name}`
    or in python: help(cdo.{name})
    """

    def __call__(self, input1:str=None, input2:str=None, input3:str=None):
        """3 input files are allowed via the decorator's arguments.
        `input1`, `input2` and `input3`. One of these may be omited. The omitted input
        file is substituted with the cdo function's `input` keyword argument.
        """
        if (input1 is input2 is None)|(input1 is input3 is None)|(input2 is input3 is None):
            raise ValueError("Only one of input1, input2 or input3 may be blank.")
        def decorator(cdo_func:FunctionType)->FunctionType:
            @functools.wraps(cdo_func)
            def wrapper(*args, **kwargs):
                if input1 is None:
                    # Python looks ahead for variable definitions within a scope to see if they
                    # are global or local. So I need to use new local-only variable names.
                    inputfile1 = kwargs['input']
                    inputfile2 = input2
                    inputfile3 = input3
                elif input2 is None:
                    inputfile1 = input1
                    inputfile2 = kwargs['input']
                    inputfile3 = input3
                elif input3 is None:
                    inputfile1 = input1
                    inputfile2 = input2
                    inputfile3 = kwargs['input']
                kwargs['input'] = f'-{self._name} {inputfile1} {inputfile2} {inputfile3}'
                return cdo_func(*args, **kwargs)
            return wrapper
        return decorator


def get_str(cdo_func:FunctionType, *args, **kwargs)->str:
    """Return the resulting CDO command chain for `cdo_func` as a string.
    """
    return cdo_func(lambda input: input)(*args, **kwargs)


# Instantiate all decorator functions
_global_vars = globals()
for name in _single_input_cdo_commands:
    _global_vars.update({'cdo_'+name: CdoSingleInputFunction(name)})
for name in _two_inputs_cdo_commands:
    _global_vars.update({'cdo_'+name: CdoTwoInputsFunction(name)})
for name in _arg_cdo_commands:
    _global_vars.update({'cdo_'+name: CdoArgInputsFunction(name)})
for name in _three_inputs_cdo_commands:
    _global_vars.update({'cdo_'+name: CdoThreeInputsFunction(name)})
for name in _arg_two_inputs_cdo_commands:
    _global_vars.update({'cdo_'+name: CdoArgTwoInputsFunction(name)})

del name

# Pdoc variables for generating documentation.
__pdoc__ = {name: True for name, klass in globals().items()
        if name.startswith('cdo_') and isinstance(klass, type)}
__pdoc__.update({f'{name}.{member}': True for name, klass in globals().items()
        if isinstance(klass, type) for member in klass.__dict__.keys()
        if member not in {'__module__','__dict__','__weakref__','__doc__','__annotations__'}})

'''BSD 2-Clause License

Copyright (c) 2021, lou059 
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''

